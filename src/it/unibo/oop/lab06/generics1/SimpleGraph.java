package it.unibo.oop.lab06.generics1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SimpleGraph<N> implements Graph<N> {

	private final Map<N, Set<N>> edges;
	
	public SimpleGraph() {
		this.edges = new HashMap<N, Set<N>>();
	}
	
	@Override
	public void addNode(final N node) {
		this.edges.putIfAbsent(node, new HashSet<N>());
	}

	@Override
	public void addEdge(final N source, final N target) {
		this.edges.get(source).add(target);
	}

	@Override
	public final Set<N> nodeSet() {
		return Collections.unmodifiableSet(this.edges.keySet());
	}

	@Override
	public final Set<N> linkedNodes(final N node) {
		return Collections.unmodifiableSet(this.edges.get(node));
	}

	@Override
	public List<N> getPath(final N source, final N target) {
		return this.getNext(source, target, new ArrayList<N>());
	}

	private List<N> getNext(final N source, final N target, final List<N> path) {
		path.add(source);
		if (this.linkedNodes(source).isEmpty()) {
			return new ArrayList<N>();
		}
		if (this.linkedNodes(source).contains(target)) {
			path.add(target);
			return path;
		}
		for (N elem : this.linkedNodes(source)) {
			return this.getNext(elem, target, path);
		}
		return new ArrayList<N>();
	}
}

